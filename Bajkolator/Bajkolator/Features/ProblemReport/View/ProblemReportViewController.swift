//
//  ProblemReportViewController.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/10/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import UIKit
import MessageUI

final class ProblemReportViewController: UIViewController {
    private let contactMail = "info@slovnaftbajk.sk"
    private let dateFormatter = DateFormatter(withFormat: "dd.MM.YYYY HH:mm:ss", locale: "sk")

    var controller: DetailController?

    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var bikeNumberField: UITextField!
    @IBOutlet weak var reasonField: UITextField!
    @IBOutlet weak var sendMailButton: UIButton!
    @IBOutlet weak var holderView: UIView!

    override func viewDidLoad() {
        holderView.layer.cornerRadius = 20
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(gestureRecognizer)
        setupView()
    }

    @IBAction func bikeNumberChanged(_ sender: Any) { checkSendButton() }

    @IBAction func reasonChanged(_ sender: Any) { checkSendButton() }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func send() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([contactMail])
        composeVC.setSubject("Problém na stanici \(stationNameLabel.text ?? "")")
        composeVC.setMessageBody(mailBody, isHTML: false)

        present(composeVC, animated: true, completion: nil)
    }

    private var mailBody: String {
        return "Dobrý deň,\n na stanici \(stationNameLabel.text ?? "")" +
            " bol zaznamenaný problém s bicyklom č.: \(bikeNumberField.nonNilText)" +
            " dňa \(dateFormatter.string(from: Date())).\n\n" +
            "Popis problému: \n\(reasonField.nonNilText)"
    }

    private func setupView() {
        if let stationNumber = controller?.stationNumber, let title = controller?.station?.title {
            stationNameLabel.text = "\(stationNumber) - \(title)"
        }
    }

    private func checkSendButton() {
        sendMailButton.isEnabled = !bikeNumberField.nonNilText.isEmpty && !reasonField.nonNilText.isEmpty
    }
}

extension ProblemReportViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(
            animated: false,
            completion: { [weak self] in self?.dismiss(animated: false, completion: nil) }
        )
    }
}

private extension UITextField {
    var nonNilText: String { return text ?? "" }
}
