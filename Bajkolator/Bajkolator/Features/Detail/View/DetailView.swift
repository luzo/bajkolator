//
//  DetailView.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/8/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import UIKit

final class DetailView: UIView {
    weak var presentationView: UIViewController?
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var bikeCountDescriptionLabel: UILabel!
    @IBOutlet weak var bikeCountLabel: UILabel!
    @IBOutlet weak var stationNumberDescriptionLabel: UILabel!
    @IBOutlet weak var stationNumberLabel: UILabel!
    @IBOutlet weak var refreshLabel: UILabel!

    private let dateFormatter = DateFormatter(withFormat: "dd.MM. HH:mm:ss", locale: "sk")
    private var observer: Any?
    var controller: DetailController? { didSet { setupView() } }

    private func setupView() {
        layer.cornerRadius = 20
        observer = controller?.stationReloaded.observe { [weak self] in self?.setupView() }

        guard let station = controller?.station else { return }
        stationNameLabel.text = station.title
        bikeCountLabel.text = "\(station.bikes)/\(station.docks)"
        stationNumberLabel.text = "\(station.stationNumber)"
        //TODO: check later whether they start to send these values again
//        refreshLabel.text = "\(station.refresh.flatMap(dateFormatter.string(from:)) ?? "N/A")"
        refreshLabel.isHidden = true
    }

    @IBAction func close() {
        removeFromSuperview()
    }

    @IBAction func navigate() {
        let alertController = UIAlertController(
            title: "Zvoľte aplikáciu pre navigáciu", message: nil, preferredStyle: .actionSheet
        )
        controller?.navigationUrls.forEach { [weak self] title, url in
            guard let url = url else { return }

            alertController.addAction(UIAlertAction(title: title, style: .default, handler: { [weak self] _ in
                self?.controller?.openNavigationLink(url)
            }))
        }
        alertController.addAction(UIAlertAction(title: "Zrušiť", style: .destructive, handler: nil))

        presentationView?.present(alertController, animated: true, completion: nil)
    }

    @IBAction func reportProblem() {
        let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: String(describing: ProblemReportViewController.self))
        (viewController as? ProblemReportViewController)?.controller = controller
        presentationView?.present(viewController, animated: true, completion: nil)
    }
}
