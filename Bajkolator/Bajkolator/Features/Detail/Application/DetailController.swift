//
//  DetailController.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/8/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import UIKit

enum NavigationService {
    case hereMaps
    case google
}

protocol DetailController {
    var stationNumber: Int { get set }
    var station: Station? { get }
    var stationReloaded: Observable { get }
    var shouldShowNavigationOptions: Observable { get }
    func openNavigationLink(_ url: URL)
    var navigationUrls: [(String, URL?)] { get }
}

final class DetailControllerImpl: DetailController {
    var navigationUrls: [(String, URL?)] {
        guard let station = station else { return [] }

        let googleLink = URL(string: String.init(format: googleNavLink, "\(station.lat)", "\(station.lng)"))
        let externalGoogleLink = URL(string: String.init(format: googleExternalNavLink, "\(station.lat)", "\(station.lng)"))
        let hereLink = URL(string: String.init(format: hereNavLink, "\(station.lat)", "\(station.lng)"))
        let appleLink = URL(string: String.init(format: appleNavLink, "\(station.lat)", "\(station.lng)"))

        return [
            ("Apple Maps", appleLink),
            ("Google Maps", googleLink.flatMap { UIApplication.shared.canOpenURL($0) ? $0 : externalGoogleLink }),
            ("HERE WeGo", hereLink),
        ].filter { _, url in url.flatMap { UIApplication.shared.canOpenURL($0) } ?? false }
    }
    var stationNumber: Int = -1
    var station: Station? {
        guard stationNumber != -1 else { return nil }
        return mapController.stations.filter { $0.stationNumber == stationNumber }.first
    }
    var stationReloaded: Observable { return _stationReloaded }
    var shouldShowNavigationOptions: Observable { return _shouldShowNavigationOptions }

    private let hereNavLink = "https://share.here.com/r/mylocation/%@,%@?m=w&t=normal"
    private let googleNavLink = "comgooglemaps://?saddr=&daddr=%@,%@&directionsmode=walking"
    private let googleExternalNavLink = "https://www.google.co.in/maps/dir/?saddr=&daddr=%@,%@&directionsmode=walking"
    private let appleNavLink = "http://maps.apple.com/?daddr=%@,%@&dirflg=w"
    private let _shouldShowNavigationOptions = ObservableCenter()
    private let _stationReloaded = ObservableCenter()
    private var mapController: MapController = MapControllerImpl.instance
    private var observer: Any?

    init() {
        observer = mapController.stationsLoaded.observe { [weak self] in self?._stationReloaded.fire() }
    }

    func openNavigationLink(_ url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
