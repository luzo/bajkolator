//
//  ViewController.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/6/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit
import Reachability

final class IndexedGMSMarker: GMSMarker {
    var index: Int?
}

final class UserMarker: GMSMarker {
    var index: Int?
}

final class MapViewController: UIViewController {

    private var mapController: MapController = MapControllerImpl.instance
    private var observers: [Any?] = []
    private let currentLocationMarker = UserMarker(position: CLLocationCoordinate2D(latitude: 0, longitude: 0))
    private let locationManager = CLLocationManager()
    private var isFirstUpdate = true
    private var zoom: Float = 16.0
    private var timer: Timer?
    private lazy var stationDetailView: DetailView? = {
        let detailView = Bundle.main.loadNibNamed(
            String(describing: DetailView.self),
            owner: nil, options: nil
        )?.first as? DetailView
        return detailView
    }()
    private let reachability = Reachability()

    private lazy var reachabilityView: UIView = {
        let holderView = UIView(frame: .zero)
        holderView.backgroundColor = #colorLiteral(red: 0.7980567683, green: 0.007843137255, blue: 0.1450980392, alpha: 1)
        let label = UILabel(frame: .zero)
        holderView.addSubview(label)
        label.text = "Žiadne pripojenie na internet"
        label.textColor = .white
        label.textAlignment = .center

        label.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        return holderView
    }()
    private var mapView: GMSMapView!

    @IBOutlet weak var currentLocationButton: UIView!

    override func loadView() {
        super.loadView()

        createMap()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationLogo()
        observers = [
            mapController.stationsLoaded.observe { [weak self] in self?.redrawStations() },
            mapController.stationsLoadingFailed.observe { [weak self] in
                let alertController = UIAlertController(
                    title: "Nepodarilo sa aktualizovať dáta aplikácie", message: nil, preferredStyle: .actionSheet
                )
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

                DispatchQueue.main.async { [weak self] in
                    self?.present(alertController, animated: true, completion: nil)

                }
            }
        ]

        DispatchQueue(label: "reachability", qos: .background).async { [weak self] in
            self?.observeReachability()
        }

        currentLocationMarker.icon = UIImage(imageLiteralResourceName: "currentLocation")
        configureLocationManager()
        timer = Timer.scheduledTimer(withTimeInterval: 45, repeats: true) { [weak self] _ in
            self?.mapController.reloadData()
        }
        mapController.reloadData()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let camera = mapView.camera
        mapView.removeFromSuperview()
        mapView = createMap(withFrame: CGRect(origin: .zero, size: size), withCamera: camera)
        mapView.delegate = self
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
        redrawStations()
    }

    private func setNavigationLogo() {
        let logo = UIImageView(image: UIImage(named: "logo"))
        logo.layer.cornerRadius = 10
        logo.layer.masksToBounds = true

        navigationItem.titleView = logo
    }

    private func createMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: zoom)
        mapView = createMap(withFrame: view.frame, withCamera: camera)
        mapView.delegate = self
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
    }

    private func createMap(withFrame frame: CGRect, withCamera camera: GMSCameraPosition) -> GMSMapView {
        return GMSMapView.map(withFrame: frame, camera: camera)
    }

    private func configureLocationManager() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 3
        locationManager.startUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.activityType = .fitness
        locationManager.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.startUpdatingLocation()
    }
}

extension MapViewController {

    @IBAction func showPins(_ sender: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: String(describing: PinsViewController.self))
        present(viewController, animated: true, completion: nil)
    }

    @IBAction func refresh(_ sender: Any) {
        mapController.reloadData()
    }

    @IBAction func showCurrentLocation(_ sender: Any) {
        let currentPosition = currentLocationMarker.position
        updateCamera(toLocation: CLLocation(latitude: currentPosition.latitude, longitude: currentPosition.longitude))
    }
}

extension MapViewController {

    private func observeReachability() {
        guard let reachability = reachability else { return }

        reachability.whenReachable = { [weak self] _ in
            self?.showNetworkView(false)
            self?.mapController.reloadData()
        }
        reachability.whenUnreachable = { [weak self] _ in self?.showNetworkView(true) }

        try? reachability.startNotifier()
    }

    private func showNetworkView(_ show: Bool) {
        if show {
            view?.addSubview(reachabilityView)
            reachabilityView.snp.makeConstraints { (make) in
                make.top.equalTo(view.snp_topMargin)
                make.leading.equalToSuperview()
                make.width.equalToSuperview()
                make.height.equalTo(50)
            }
        } else {
            reachabilityView.removeFromSuperview()
        }
    }

    private func color(forState state: State) -> UIColor {
        switch state {
        case .empty: return #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
        case .low: return #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        case .ok: return #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        case .high: return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        case .full: return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        }
    }

    private func redrawStations() {
        mapView.clear()
        mapController.stations.enumerated().forEach { index, station in
            let marker = IndexedGMSMarker(position: CLLocationCoordinate2D(latitude: station.lat, longitude: station.lng))
            marker.index = index
            marker.icon = createImage(forStation: station)
            marker.map = mapView
        }
        currentLocationMarker.map = mapView
    }

    private func createImage(forStation station: Station) -> UIImage? {
        let view = Bundle.main.loadNibNamed(String(describing: MarkerView.self), owner: nil, options: nil)?.first as? MarkerView
        view?.setText("\(station.bikes)")
        view?.backgroundColor = .black
        let size = view?.bounds.size ?? .zero
        let innerSize = view?.innerView.bounds.size.height ?? 0
        view?.innerView.layer.cornerRadius = innerSize/2
        view?.innerView.backgroundColor = color(forState: station.state)

        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        defer { UIGraphicsEndImageContext() }

        if let context = UIGraphicsGetCurrentContext() {
            let radius = size.height / 2
            let path = UIBezierPath(roundedRect: CGRect(origin: .zero, size: size), cornerRadius: radius)
            context.addPath(path.cgPath)
            context.clip()
            view?.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()

            return image
        }

        return nil
    }

    private func updateCamera(toLocation location: CLLocation) {
        guard let mapView = mapView else { return }

        let camera = GMSCameraPosition.camera(
            withLatitude: location.coordinate.latitude,
            longitude: location.coordinate.longitude,
            zoom: zoom
        )

        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
}

extension MapViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let marker = marker as? IndexedGMSMarker, let index = marker.index, let detailView = stationDetailView else { return false }

        let detailController = mapController.deferDetailController(forIndex: index)
        detailView.controller = detailController
        detailView.presentationView = self
        
        if !view.subviews.contains(detailView) {
            view.addSubview(detailView)
            detailView.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
                make.leading.greaterThanOrEqualToSuperview().inset(20)
                make.top.greaterThanOrEqualToSuperview().inset(20)
            })
        }

        return true
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        zoom = position.zoom
    }
}

extension MapViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = locations.last, let mapView = mapView else { return }

        if isFirstUpdate {
            updateCamera(toLocation: location)
            isFirstUpdate = !isFirstUpdate
        }
        currentLocationMarker.position = location.coordinate
        currentLocationMarker.map = mapView

        if mapView.isHidden { mapView.isHidden = false }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied, .notDetermined:
            manager.stopUpdatingLocation()
        case .authorizedAlways, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {}
}
