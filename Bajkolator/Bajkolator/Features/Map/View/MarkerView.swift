//
//  MarkerView.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/7/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import UIKit

final class MarkerView: UIView {
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var innerView: UIView!

    func setText(_ text: String) {
        countLabel.text = text
    }
}
