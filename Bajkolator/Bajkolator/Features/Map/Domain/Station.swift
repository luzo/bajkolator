//
//  Station.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/6/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import Foundation

enum State {
    case low
    case ok
    case high
    case full
    case empty
}

struct Station
{
    let bikes: Int
    let docks: Int
    let ebikes: Int
    let state: State
    let lat: Double
    let lng: Double
    let refresh: Date?
    let reg: Int
    let stationNumber: Int
    let title: String
}

class StationConverter {
    private let dateFormatter = DateFormatter(withFormat: "YYYY-MM-dd HH:mm:ss", locale: "sk")

    func convert(input: StationDto, stationNames: [Int: String]) -> Station {
        let stationNumber = input.station_nr.flatMap(Int.init) ?? 0

        return Station(
            bikes: input.bikes ?? 0,
            docks: input.docks.flatMap(Int.init) ?? 0,
            ebikes: input.ebikes ?? 0,
            state: convertState(fromImgName: input.img ?? ""),
            lat: input.lat.flatMap(Double.init) ?? 0,
            lng: input.lng.flatMap(Double.init) ?? 0,
            refresh: input.refresh.flatMap(dateFormatter.date(from:)),
            reg: input.reg.flatMap(Int.init) ?? 0,
            stationNumber: stationNumber,
            title: stationNames[stationNumber] ?? ""
        )
    }

    private func convertState(fromImgName name: String) -> State {
        let imageName = name.split(separator: "/").last?.split(separator: ".").first
        switch imageName {
        case "marker-station-low": return .low
        case "marker-station-high": return .high
        case "marker-station-ok": return .ok
        case "marker-station-overload": return .full
        case "marker-station-empty": return .empty
        default: return .empty
        }
    }
}
