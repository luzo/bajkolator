//
//  StationOutputDto.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/6/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import ObjectMapper

struct StationsOutputDto: Mappable {
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        points <- map["points"]
    }

    var points: [StationDto]?
}

struct StationDto: Mappable
{
    var bikes: Int?
    var docks: String?
    var ebikes: Int?
    var img: String?
    var lat: String?
    var lng: String?
    var refresh: String?
    var reg: String?
    var station_nr: String?

    init?(map: Map) {}

    mutating func mapping(map: Map) {
        bikes <- map["bikes"]
        docks <- map["docks"]
        ebikes <- map["ebikes"]
        img <- map["img"]
        lat <- map["lat"]
        lng <- map["lng"]
        refresh <- map["refresh"]
        reg <- map["reg"]
        station_nr <- map["station_nr"]
    }
}
