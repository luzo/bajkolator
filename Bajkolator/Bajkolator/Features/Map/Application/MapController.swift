//
//  MapController.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/6/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import Foundation
import SwiftSoup
import JavaScriptCore
import ObjectMapper
import CoreLocation

protocol MapController: AnyObject {
    static var instance: MapController { get }
    func reloadData()
    var stations: [Station] { get }
    var stationsLoaded: Observable { get }
    var stationsLoadingFailed: Observable { get }
    func deferDetailController(forIndex index: Int) -> DetailController
}

final class MapControllerImpl {
    private let key = "bajk-stations"
    private let keyStations = "bajk-stationNames"
    static let instance: MapController = MapControllerImpl()

    var userLocation: CLLocation?
    var stations = [Station]()
    var stationsLoaded: Observable { return _stationsLoaded }
    var stationsLoadingFailed: Observable { return _stationsLoadingFailed }

    private let _stationsLoaded = ObservableCenter()
    private let _stationsLoadingFailed = ObservableCenter()
    private let mapPage = "mapa-stanic"
    private let markerDataName = "gMap.markerData"
    private let converter = StationConverter()
    private var isLoading = false

    private init() {}

    private func parse(html: String?) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard
                let weakSelf = self,
                let html = html,
                let doc = try? SwiftSoup.parse(html),
                let scripts = try? doc.getElementsByTag("script"),
                let markersData = scripts.map({ $0.data() }).filter({ $0.contains(weakSelf.markerDataName) }).first,
                let startIndex = markersData.range(of: weakSelf.markerDataName)?.lowerBound,
                let endIndex = markersData.range(of: "}];")?.upperBound,
                let object = weakSelf.createJsObject(fromString: markersData.substring(with: startIndex..<endIndex))
            else {
                self?.retrieveDataFromCache()
                self?._stationsLoadingFailed.fire()
                self?.isLoading = false

                return
            }

            weakSelf.obtainStationsFromJsObject(object, stationNames: weakSelf.obtainStationNames(doc: doc))
        }
    }

    private func retrieveDataFromCache() {
        if
            let data = UserDefaults.standard.data(forKey: key),
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDict = jsonObject as? [String: Any],
            let stationNamesData = UserDefaults.standard.data(forKey: keyStations),
            let names = try? PropertyListDecoder().decode([Int: String].self, from: stationNamesData)
        {
            obtainStationsFromJsonObject(object: jsonDict, stationNames: names)
        }
    }

    private func createJsObject(fromString string: String) -> [String: Any]? {
        let mapPointsObtainScriptString = "var gMap = new Object();" + "gMap.markerData = [];" + string
        let jsContext = JSContext.init()
        _ = jsContext?.evaluateScript(mapPointsObtainScriptString)

        return jsContext?.globalObject?.toDictionary() as? [String: Any]
    }

    private func obtainStationsFromJsObject(_ object: [String: Any]?, stationNames: [Int: String]) {
        isLoading = false

        if
            let object = object,
            let markerData = object["gMap"] as? [String: Any],
            let points = (markerData["markerData"] as? [Any])?.filter({ $0 is [String: Any]})
        {
            let jsonObject = ["points": points]
            if let data = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) {
                UserDefaults.standard.setValue(data, forKey: key)
            }

            obtainStationsFromJsonObject(object: jsonObject, stationNames: stationNames)
        } else {
            _stationsLoadingFailed.fire()
        }
    }

    private func obtainStationsFromJsonObject(object: [String: Any], stationNames: [Int: String]) {
        let output = Mapper<StationsOutputDto>().map(JSON: object)?.points ?? []
        stations = output.map { converter.convert(input:$0, stationNames: stationNames) }

        DispatchQueue.main.async { [weak self] in self?._stationsLoaded.fire() }
    }

    private func obtainStationNames(doc: Document) -> [Int: String] {
        var stationNames = [Int: String]()
        try? doc.select("#cmeStationInfo > tbody").first()?.children().forEach {
            if
                let stationNumber = try? $0.getElementsByAttributeValue("data-title", "Číslo stanice").text(),
                let number = Int(stationNumber),
                let stationName = try? $0.getElementsByAttributeValue("data-title", "Názov stanice").text() {

                stationNames[number] = stationName
            }
        }

        if let encoded = try? PropertyListEncoder().encode(stationNames) {
            UserDefaults.standard.setValue(encoded, forKey: keyStations)
        }

        return stationNames
    }
}

extension MapControllerImpl: MapController {
    func reloadData() {
        guard !isLoading else { return }
        isLoading = true

        let pageToLoad = ApiConfiguration.instance.baseUrl.appendingPathComponent(mapPage)

        DispatchQueue.main.async { [weak self] in
            self?.parse(html: (try? Data(contentsOf: pageToLoad)).flatMap { String(data: $0, encoding: .utf8)})
        }
    }

    func deferDetailController(forIndex index: Int) -> DetailController {
        let controller = DetailControllerImpl()
        controller.stationNumber = stations[index].stationNumber

        return controller
    }
}
