//
//  ApiConfiguration.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/6/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import Foundation

class ApiConfiguration {
    static let instance = ApiConfiguration()
    let baseUrl = URL(string: "https://slovnaftbajk.sk/")!

    private init() {}
}
