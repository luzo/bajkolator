//
//  PinsViewController.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/8/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import UIKit

final class PinsViewController: UIViewController {
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var pinsStackView: UIStackView!
    @IBOutlet weak var pinField: UITextField!
    @IBOutlet weak var addPinButton: UIButton!
    @IBOutlet weak var noPinsLabel: UILabel!

    private let pinsController: PinsController = PinsControllerImpl()

    override func viewDidLoad() {
        holderView.layer.cornerRadius = 20
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(gestureRecognizer)
        showPins()
    }

    @IBAction func enteredPinChanged(_ sender: Any) {
        let text = pinField.text ?? ""
        addPinButton.isEnabled = !text.isEmpty && text.count == 8
    }

    @IBAction func addPin(_ sender: Any) {
        dismissKeyboard()
        guard let text = pinField.text else { return }

        pinsController.addPin(pin: text)
        noPinsLabel.isHidden = !pinsController.pins.isEmpty
        showPins()
        pinField.text = nil
    }

    @IBAction func removePin(_ sender: Any) {
        pinsController.removeLastPin()
        showPins()
    }

    @objc func dismissKeyboard() {
        pinField.resignFirstResponder()
    }

    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }

    func showPins() {
        pinField.isEnabled = pinsController.pins.count <= 4

        pinsStackView.arrangedSubviews.forEach {
            pinsStackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }

        noPinsLabel.isHidden = !pinsController.pins.isEmpty
        pinsController.pins.forEach {
            let label = UILabel(frame: .zero)
            label.text = $0.enumerated().map({ $0.offset % 3 == 0 ? " \($0.element)" : "\($0.element)" }).joined()
            label.textColor = .black
            label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
            label.textAlignment = .center

            pinsStackView.addArrangedSubview(label)
            label.snp.makeConstraints({ (make) in
                make.height.equalTo(30)
                make.width.equalToSuperview()
            })
        }
    }
}
