//
//  PinsController.swift
//  Bajkolator
//
//  Created by Lubos Lehota on 11/8/18.
//  Copyright © 2018 LubosLehota. All rights reserved.
//

import Foundation

protocol PinsController {
    var pins: [String] { get }
    func addPin(pin: String)
    func removeLastPin()
}

final class PinsControllerImpl: PinsController {
    private let key = "bajk-pins"
    var pins: [String] = []

    init() {
        pins = UserDefaults.standard.array(forKey: key) as? [String] ?? []
    }

    func addPin(pin: String) {
        pins.append(pin)
        UserDefaults.standard.set(pins, forKey: key)
    }

    func removeLastPin() {
        guard pins.count > 0 else { return }
        pins.removeLast()
        UserDefaults.standard.set(pins, forKey: key)
    }
}
